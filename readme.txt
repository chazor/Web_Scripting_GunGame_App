Die App wurde dafür gemacht Gun Game in Counter Strike Go Wettkämpfen zu simulieren. Dabei soll das "Steam-Overlay"
verwendet werden, welches einen Internet Browser zur Verfügung stellt. Die App greift dabei zu keiner Zeit in das Spiel ein.
In der jetzigen Version ist es möglich zwischen zwei Arten von Gun Game zu wählen. Das klassische Gun Game, dort startet
man mit schlechten Waffen und bekommt bessere Waffen immer dann wenn man einen Gegner eliminiert. Und das zur Zeit
populärere Gun Game mit invertierter Reihenfolge. Also die besten Waffen zuerst und die schlechteren am Ende.
Diese Waffen können im Wettkampfmodus nur am Anfang der Runde gekauft werden.
Zur Zeit ist es nur möglich sich die Dauer der einzelnen Durchläufe und die Anzahl der erfolgreich abgeschlossenen Durchläufe
anzeigen zu lassen und das auch nur lokal auf dem eigenen System. Durch die Syntax ist es aber möglich diese Elemente
 später auzulagern und in ein JSON File zu speichern. Auch sind schon Funktionen vorhanden, die es möglich machen, genau aufzuzeigen
 bis zu welcher Waffe man es geschaft hat. Es wurde hier aber noch keine Speicher und Anzeige Funktion erstellt, weshalb
 die Funktion noch nicht genutzt wird. Probleme gibt es noch bei der Ausgabe der Statistik. Hier muss die Seite aktualisiert
 werden um die Angaben anzuzeigen, wenn man einen Durchlauf abgeschlossen hat.
 
 node server adress: http://localhost:8100/index.html