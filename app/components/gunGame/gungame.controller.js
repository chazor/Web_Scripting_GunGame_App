(function () {
    angular.module('ggApp').controller('GunGameCtrl', function (persistenceFactory, gunGameFactory, $scope) {
        var self = this;

        self.date = new Date();
        //Objekt Array mit den verschiedenen Waffen aus Counter Strike GO
        self.weapons = weapons;
        self.count = 0;
        self.countReversed = self.weapons.length - 1;
        self.originalData = angular.copy(self.weapons);
        self.weaponsReverse = [];
        self.startTimestamp = null;
        self.endTimestamp = null;
        self.executed = false;
        //Anzahl bisheriger kompletter Durchläufe
        self.timesFinished=persistenceFactory.load("timesFinished");
        //Dauer der einzelnen Durchläufe
        self.playThroughTime=JSON.parse(persistenceFactory.load("durationArray"));
        self.isVisible=false;

        self.nextWeapon = nextWeapon;
        self.nextWeaponReversed = nextWeaponReversed;
        self.resetWeapons = resetWeapons;
        self.setWeaponOrderReverse = setWeaponOrderReverse;
        self.getActiveWeapon = getActiveWeapon;
        self.toggleShow=toggleShow;

        //Funktion für das umschalten zwischen sichtbar und unsichtbar von z.B. einem DIV
        function toggleShow(){
            self.isVisible=self.isVisible ? false : true;
        }

        //gibt das Waffenobjekt aus auf welches der Indexzeiger in dem Moment zeigt
        function getActiveWeapon(weaponset) {
            for (var i = 0; i < weaponset.length + 1; i++) {
                if (weaponset[i].state == "active") {
                    return weaponset[i];
                }
            }
        }

        /*Hier wird das Messer und die Waffe "Zeus" bei der invertierten Gun Game Anzeige an die letzte
         * und Vorletze stelle gesetzt. Das war notwendig da durch die Umkehrung diese zwei Waffen ansonsten an erster
          * und Zweiter Stelle gestanden hätten und das so nicht üblich ist. (Der Abschluss mit dem Messer ist üblich)*/
        function setWeaponOrderReverse() {
            self.weaponsReverse = gunGameFactory.changeArrayPosition(self.weapons, 32, 0);
            self.weaponsReverse = gunGameFactory.changeArrayPosition(self.weaponsReverse, 33, 0);
        }

        /* Schaltet per Klick den Fokus auf die nächste Waffe, wenn der Durchlauf mit Erreichen der letzten Waffe
         * beendet ist, wird die Dauer des Durchlaufes lokal gespeichert */
        function nextWeapon() {
            if (self.count === 0) {
                self.startTimestamp = Date.now();

            }
            if (self.count < self.weapons.length) {
                self.weapons[self.count].state = "active";
                if (self.count > 0) {
                    self.weapons[self.count - 1].state = "finished";
                }
                self.count++;
            }
            if (self.count === self.weapons.length) {
                self.weapons[self.count - 1].state = "finished";

                if (!self.executed) {
                    self.endTimestamp = Date.now();
                    self.executed = true;


                    var duration = ((gunGameFactory.getDuration(self.startTimestamp, self.endTimestamp)));
                    persistenceFactory.saveDuration(duration);


                    console.log(persistenceFactory.load("durationArray"), persistenceFactory.load("timesFinished"));


                }
            }

        }
        /*Fast die selbe Funktion wie nextWeapon(). War aber notwendig, da durch die Invertierung des Arrays die Syntax
         * an vielen Stellen angepasst werden musste. */
        function nextWeaponReversed() {
            if (self.countReversed === self.weaponsReverse.length - 1) {
                self.startTimestamp = Date.now();

            }

            if (self.countReversed >= 0) {
                self.weaponsReverse[self.countReversed].state = "active";
                if (self.countReversed < self.weapons.length - 1) {
                    self.weaponsReverse[self.countReversed + 1].state = "finished";
                }
                self.countReversed--;
            }
            if (self.countReversed === -1) {
                self.weaponsReverse[self.countReversed + 1].state = "finished";
                if (!self.executed) {
                    self.endTimestamp = Date.now();

                    self.executed = true;
                    var duration = ((gunGameFactory.getDuration(self.startTimestamp, self.endTimestamp)));
                    persistenceFactory.saveDuration(duration);


                    console.log(persistenceFactory.load("durationArray"), persistenceFactory.load("timesFinished"));
                }

            }
        }
        /*Setzt die einzelnen Werte auf ihren ursprünglichen Wert zurück */
        function resetWeapons() {
            /*angular.copy(self.originalData, self.weapons);
             angular.copy(self.originalData, self.weaponsReverse);*/

            for (var i = 0; i < self.weapons.length; i++) {
                self.weapons[i].state = "not_set";
            }
            for (var i = 0; i < self.weaponsReverse.length; i++) {
                self.weaponsReverse[i].state = "not_set";
            }

            self.count = 0;
            self.countReversed = self.weapons.length - 1;
            self.startTimestamp = null;
            self.endTimestamp = null;
            self.executed = false;
        }


    });


    var weapons = [
        {id: 0, name: 'USP', icon: 'img/usp.png', price: "not_yet_used", state: "not_set"},
        {id: 1, name: 'P2000', icon: 'img/p2000.png', price: "not_yet_used", state: "not_set"},
        {id: 2, name: 'Glock', icon: 'img/glock.png', price: "not_yet_used", state: "not_set"},
        {id: 3, name: 'P250', icon: 'img/p250.png', price: "not_yet_used", state: "not_set"},
        {id: 4, name: 'Five-Seven', icon: 'img/five_seven.png', price: "not_yet_used", state: "not_set"},
        {id: 5, name: 'TEC-9', icon: 'img/tec9.png', price: "not_yet_used", state: "not_set"},
        {id: 6, name: 'CZ75-Auto', icon: 'img/cz.png', price: "not_yet_used", state: "not_set"},
        {id: 7, name: 'Duals', icon: 'img/duals.png', price: "not_yet_used", state: "not_set"},
        {id: 8, name: 'Deagle', icon: 'img/deagle.png', price: "not_yet_used", state: "not_set"},
        {id: 9, name: 'Revolver', icon: 'img/revolver.png', price: "not_yet_used", state: "not_set"},
        {id: 10, name: 'MAC-10', icon: 'img/mac10.png', price: "not_yet_used", state: "not_set"},
        {id: 11, name: 'MP-9', icon: 'img/mp9.png', price: "not_yet_used", state: "not_set"},
        {id: 12, name: 'UMP', icon: 'img/ump.png', price: "not_yet_used", state: "not_set"},
        {id: 13, name: 'Bizon', icon: 'img/bizon.png', price: "not_yet_used", state: "not_set"},
        {id: 14, name: 'MP7', icon: 'img/mp7.png', price: "not_yet_used", state: "not_set"},
        {id: 15, name: 'P90', icon: 'img/p90.png', price: "not_yet_used", state: "not_set"},
        {id: 16, name: 'Nova', icon: 'img/nova.png', price: "not_yet_used", state: "not_set"},
        {id: 17, name: 'XM-104', icon: 'img/xm104.png', price: "not_yet_used", state: "not_set"},
        {id: 18, name: 'MAG-7', icon: 'img/mag7.png', price: "not_yet_used", state: "not_set"},
        {id: 19, name: 'Sawed Off', icon: 'img/sawedoff.png', price: "not_yet_used", state: "not_set"},
        {id: 20, name: 'Famas', icon: 'img/famas.png', price: "not_yet_used", state: "not_set"},
        {id: 21, name: 'Galil', icon: 'img/galil.png', price: "not_yet_used", state: "not_set"},
        {id: 22, name: 'M4', icon: 'img/m4a4.png', price: "not_yet_used", state: "not_set"},
        {id: 23, name: 'AK-47', icon: 'img/ak47.png', price: "not_yet_used", state: "not_set"},
        {id: 24, name: 'AUG', icon: 'img/aug.png', price: "not_yet_used", state: "not_set"},
        {id: 25, name: 'SG-553', icon: 'img/sg.png', price: "not_yet_used", state: "not_set"},
        {id: 26, name: 'M249', icon: 'img/m249.png', price: "not_yet_used", state: "not_set"},
        {id: 27, name: 'Negev', icon: 'img/negev.png', price: "not_yet_used", state: "not_set"},
        {id: 28, name: 'Scout', icon: 'img/ssg.png', price: "not_yet_used", state: "not_set"},
        {id: 29, name: 'AWP', icon: 'img/awp.png', price: "not_yet_used", state: "not_set"},
        {id: 30, name: 'SCAR-20', icon: 'img/scar20.png', price: "not_yet_used",state: "not_set"},
        {id: 31, name: 'G3SG1', icon: 'img/g3sg1.png', price: "not_yet_used", state: "not_set"},
        {id: 32, name: 'Zeus', icon: 'img/zeus.png', price: "not_yet_used", state: "not_set"},
        {id: 33, name: 'Knife', icon: 'img/knife.png', price: "not_yet_used", state: "not_set"}
    ];


})();