
(function () {
    angular.module('ggApp').config(['$routeProvider' ,function ($routeProvider) {
        $routeProvider.when('/gungame',{
            template: '<gungame format="\'gungame\'">'
        }).when('/reversed',{
            template: '<gungame format="\'reversed\'">'
        }).when('/random', {
            template: '<gungame format="\'random\'">'
        }).otherwise('/gungame');

    }]);
})();