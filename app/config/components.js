/* Hauptkomponente für GG-App */
(function () {

    angular.module('ggApp').component("gungame", {

        templateUrl: 'app/components/gunGame/gungame.html',
        controller: 'GunGameCtrl',
        bindings: {
            format: '='
        },
        controllerAs:'gg'

    });



})();