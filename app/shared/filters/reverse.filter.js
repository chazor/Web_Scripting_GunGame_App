/* Wird nicht mehr genutzt, habe eine bessere Methode gefunden ein Array von hinten durchschlaufen zu lassen*/

(function(){

    angular.module('ggApp').filter('reverse', function() {
        return function(items) {
            return items.slice().reverse();
        };
    });

})();
