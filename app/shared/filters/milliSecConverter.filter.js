(function () {
    angular.module('ggApp').filter('millSecConverter', function () {

        //Filter für die Ausgabe der Zeit. Konvertiert Millisekunden in gut lesbare Form.
        return function (millseconds) {
            var oneSecond = 1000;
            var oneMinute = oneSecond * 60;
            var oneHour = oneMinute * 60;
            var oneDay = oneHour * 24;

            var seconds = Math.floor((millseconds % oneMinute) / oneSecond);
            var minutes = Math.floor((millseconds % oneHour) / oneMinute);
            var hours = Math.floor((millseconds % oneDay) / oneHour);
            var days = Math.floor(millseconds / oneDay);

            var  convTime = '';
            if (days !== 0) {
                 convTime += (days !== 1) ? (days + ' Tage') : (days + ' Tag ');
            }
            if (hours !== 0) {
                 convTime += (hours !== 1) ? (hours + ' Stunden ') : (hours + ' Stunde ');
            }
            if (minutes !== 0) {
                 convTime += (minutes !== 1) ? (minutes + ' Minuten ') : (minutes + ' Minute ');
            }
            if (seconds !== 0 || millseconds < 1000) {
                 convTime += (seconds !== 1) ? (seconds + ' Sekunden ') : (seconds + ' Sekunde ');
            }

            return  convTime;
        };
    });
})();