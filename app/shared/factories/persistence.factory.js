(function () {

    angular.module('ggApp').factory('persistenceFactory', function () {
        return {
            load: load,
            save: save,
            saveDuration:saveDuration
        };
        // Ließt einen Wert aus dem LocalStorage aus.
        function load(key) {
            return window.localStorage.getItem(key);
        }

        // Legt einen Wert im LocalStorage ab.
        function save(key, value) {
            window.localStorage.setItem(key, value);
        }
        //Speichert und aktualisiert die Dauer der einzelnen Durchläufe, außerdem speichert es die Anzahl der erfolgreichen
        //Durchläufe ab
        function saveDuration(duration) {
            if (load("durationArray") !== null) {
                var storedArray = JSON.parse(load("durationArray"));
                storedArray.push(duration);
                save("durationArray", JSON.stringify(storedArray));
            } else {
                var durationArray = [duration];
                save("durationArray", JSON.stringify(durationArray));
            }
            if (load("timesFinished") === null) {
                save("timesFinished", 1);
            } else {
                var storedCount = load("timesFinished");
                storedCount++;
                save("timesFinished", storedCount);
            }
        }

        // Erstellt den Key der zum Ablegen eines Wertes im LocalStorage benutzt
        // wird.
        /*  function createKey(value) {
         return value;
         }*/

    });
})();