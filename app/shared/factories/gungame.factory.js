(function () {

    angular.module('ggApp').factory('gunGameFactory', function () {

        var date=new Date();
        //Funktion um bestimmte Elemente auf gewünschte Positionen in einem Array zu schieben. origin = Ursprüngliche Position
        //toIndex = Position auf die das Element verschoben werden soll.
        function changeArrayPosition(array, origin, toIndex) {
            var element = array[origin];
            var clonedArray = JSON.parse(JSON.stringify(array));
            clonedArray.splice(origin, 1);
            clonedArray.splice(toIndex, 0, element);
            return clonedArray;
        }

        //gibt die Dauer aus. Wird durch Timestamps realisiert. (Einen Zahl, die die vergangenen Millisekunden seit dem
        // 1. Januar 1970 00:00:00 Weltzeit (UTC) angibt (UNIX-Zeit).
        function getDuration(timestampStart, timestampEnd){
            return timestampEnd-timestampStart;
        }

        return{
            changeArrayPosition:changeArrayPosition,
            getDuration:getDuration

        };

    })
    ;
})();
