/* Wurde genutzt, ist aber nach Umstellung nicht mehr notwendig gewesen
Die Direktive war dafür da, ein wiederholtes Klicken von Elementen zu verhindern.
 */
angular.module('ggApp').directive("clickOnce", function () {
    return {
        restrict: "A",
        link: function (scope, element, attribute) {
            var clickFunction = function () {
                scope.$eval(attribute.clickOnce);
                scope.$apply();
                element.unbind("click", clickFunction);
            };

            element.bind("click", clickFunction);
        }
    };
});
